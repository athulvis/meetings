# ![Meetings Icon](static/meetings_logo.jpg) MEETINGS 

Plans and proceedings of meetings conducted by Free Software Users' Group, Trivandrum are
preserved here. They can be accessed from links below.

## Next Meeting
```
Date   : August 20th, 2017
Time   : 2:00 PM to 5:30 PM
Venue  : SPACE, Vellayambalam
Topics :
    1. Intermediate Git (Gokul Das)
    2. Collaboration using GitLab (Gokul Das)
    3. Introduction to Python (Aswin Karuvally)
```

## Previous Meetings

### August 2017
- [Meeting on 13th](2017-08-13.md)

## Credits
**Meetings Logo:** [Scott Maxell](https://thegoldguys.blogspot.in/).
Shared from [flickr.com](https://www.flickr.com/photos/lumaxart/2181400330/in/photostream/)
under [CC BY-SA 2.0 license](https://creativecommons.org/licenses/by-sa/2.0/)

**Group Hosting:** [Free Software Community of India](http://fsci.org.in/)

